#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hiredis.h>
#include <demo.h>
unsigned int j;
redisContext *c;
redisReply *reply;
int main(int argc, char **argv) 
{
	    const char *hostname = (argc > 1) ? argv[1] : "127.0.0.1";
	    int port = (argc > 2) ? atoi(argv[2]) : 6379;
	    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	    c = redisConnectWithTimeout(hostname, port, timeout);//creating context 
	    if (c == NULL || c->err) 
	    {
		if (c) 
		{
		    printf("Connection error: %s\n", c->errstr);
		    redisFree(c);
		} 
		else 
		{
		    printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	    }

	    /* PING server */
	    reply = redisCommand(c,"PING");
	    printf("PING: %s\n", reply->str);
	    freeReplyObject(reply);
            printf("ENTER HASH YOUWANT\n");
            char *buf=malloc(100);//buffer for storing hash table
            char *hash_name=malloc(20);//buffer for storing the cmd like HGET,HSET
	    char *hash_key=malloc(20);
	    char *hash_value=malloc(50);
	    int l=0;
            printf("ENTER HASH TABLE  KEY\n");
	    scanf ("%[^\n]%*c", hash_name);//taking input upto pressing enter
	    //scanf("%s",hash_name);
	    printf("ENTER KEY \n");
	   // scanf("%s",hash_key);
	    scanf ("%[^\n]%*c", hash_key);
	    printf("ENTER HASH VALUE\n");
	    //scanf("%s",hash_value);
	    scanf ("%[^\n]%*c", hash_value);
	    snprintf(buf,100,"%s %s %s",hash_name,hash_key,hash_value);
	    //printf("LINE %d buf=%s",__LINE__,buf);
	    reply = set(c,reply,buf);
	    //set(c,reply,buf);
            printf("HMSET: %s\n",reply->str);
            freeReplyObject(reply);
            redisFree(c);
  	    return 0;  

}

