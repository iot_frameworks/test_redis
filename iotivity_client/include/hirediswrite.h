/*
 * hirediswrite.h
 *
 *  Created on: 13-Jun-2017
 *      Author: jotirling
 */

#ifndef HIREDISWRITE_H_
#define HIREDISWRITE_H_

class REDIS
{
public:
void Context(void);
void t_resource_attPrint(string hash_key, OCRepPayloadValue* repp);

};


#endif /* HIREDISWRITE_H_ */
