/*
 * headers.h
 *
 *  Created on: 09-Jun-2017
 *      Author: smartron
 */

#ifndef HEADERS_H_
#define HEADERS_H_

#include "iotivity_config.h"
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <Windows.h>
#endif
#include <string>
#include <map>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include "OCPlatform.h"
#include "OCApi.h"

#include <signal.h>
#include <ctime>

#include "samplegrpc.h"
#include "device_thing_class.h"
#include "Discovery.h"
#include "hirediswrite.h"

#endif /* HEADERS_H_ */
