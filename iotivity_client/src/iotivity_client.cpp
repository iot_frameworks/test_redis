#include "headers.h"

using namespace OC;
using namespace std;

#define  discoverying   1302
#define  getting        754
#define  posting        772
#define  observeing     1076

void Discovery_all(void);
void  Auto_Discovery(int );


extern std::map<string, t_things*> t_thingsmap;                         // t_things map
extern std::map<std::string, int >RepPayloadMap;
extern bool auto_Discovery;


struct comand JPP;


int cont=0;         // testing


void getResource(string uuid,string key)
{
        cout << "In function getResource" << endl;
        QueryParamsMap test;
        auto it_uid = t_thingsmap.find(uuid);
        if (it_uid != t_thingsmap.end())
        {
                for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
                {
                        auto it = it_uid->second->deviceVec[i]->resMap.find(key);
                        if (it != it_uid->second->deviceVec[i]->resMap.end())
                        {
                                std::cout << "get req sent :" <<endl;
                                it->second->getRepresentation( );
                        }
                }
        }
}

void postResource(string uuid,string key,string attributeName,string inputValue)
{
        attributetype attributeValue;
        OCRepresentation rep;

        auto it_uid = t_thingsmap.find(uuid);
        if (it_uid != t_thingsmap.end())
        {
                for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
                {
                        auto it = it_uid->second->deviceVec[i]->resMap.find(key);
                        if (it != it_uid->second->deviceVec[i]->resMap.end())
                        {
                                auto it1 = RepPayloadMap.find(attributeName);
                                if (it1 != RepPayloadMap.end())
                                        cout<<"Attribute: "<<it1->second<<endl;
                                else
                                    cout<<" attribute not found: "<<endl;
                                switch(it1->second)
                                {
                                case 0:
                                        cout<<"OCREP_PROP_NULL"<<endl;
                                        break;
                                case 1:
                                        cout<<"OCREP_PROP_INT"<<endl;
                                        attributeValue.i=stoi(inputValue);
                                        rep.setValue(attributeName, attributeValue.i);
                                        break;
                                case 2:
                                        cout<<"OCREP_PROP_DOUBLE"<<endl;
                                        attributeValue.d=stod(inputValue);
                                        rep.setValue(attributeName, attributeValue.d);
                                        break;
                                case 3:
                                        cout<<"OCREP_PROP_BOOL"<<endl;
                                        if(inputValue == "true")
                                                attributeValue.b=1;
                                        else if(inputValue == "false")
                                                attributeValue.b=0;
                                        rep.setValue(attributeName, attributeValue.b);
                                        break;
                                case 4:
                                        cout<<"OCREP_PROP_STRING"<<endl;
                                        attributeValue.str=inputValue;
                                        rep.setValue(attributeName, attributeValue.str);
                                        break;
                                }

                                it->second->postRepresentation(rep);
                        }
                }
        }
}



void observeResource(string uuid,string key)
{
        cout << "In function observeResource" << endl;
        QueryParamsMap test;
        auto it_uid = t_thingsmap.find(uuid);
        if (it_uid != t_thingsmap.end())
        {
            for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
            {
                    auto it = it_uid->second->deviceVec[i]->resMap.find(key);
                    if (it != it_uid->second->deviceVec[i]->resMap.end())
                    {
                        it->second->observeRepresentation( );
                    }
            }
        }
}

int convertToASCII(string letter)
{

        int m = 0;
        for (int i = 0; i < letter.length(); i++)
        {
                char x = letter.at(i);
                m+= int(x);
        }
        return m;
}

void one_time_Discovery()
{
        signal(SIGALRM, end_Discovery );
        Discovery_all();
}

void  Auto_Discovery(int sig)
{
        printf("IN Auto_Discovery\n");
        one_time_Discovery();
//      alarm(3600);
}

void *myThread_Dis(void *p)
{
        int opt=*(int *)p;

        printf("IN myThread_Dis\n");

        switch(opt)
        {
        case 1: std::cout << "in one_time_Discovery" << '\n';
                one_time_Discovery();
                break;

        case 2: signal(SIGALRM, Auto_Discovery );
                auto_Discovery=true;
                std::cout << "in Auto_Discovery" << '\n';
                alarm(1);
                break;

        case 3: signal(SIGALRM, SIG_IGN );
                auto_Discovery=false;
                std::cout << "in Disable_Auto_Discovery" << '\n';
                break;

        default: std::cout << "in one_time_Discovery" << '\n';
                one_time_Discovery();
                break;
        }


        printf("OUT myThread_Dis\n");

}

void *ResControl(void *p)
{
        int option=*(int *)p;
        string attributeName,attributeValue;
        string key,uuid;

        uuid=JPP.UUID;
        key=JPP.UUID+JPP.DevID;
        attributeName=JPP.attributeName1;
        attributeValue=JPP.attributeValue1;

        switch(option)
        {
        case getting:
                getResource(uuid,key);
                sleep(2);
                break;

        case posting:
                postResource(uuid,key,attributeName,attributeValue);
                sleep(2);
                break;

        case observeing:
                observeResource(uuid,key);
                sleep(2);
                break;


        default:
                std::cout<<"Please enter a proper option "<<std::endl;

        }
}

int main()
{


        std::cout.setf(std::ios::boolalpha);
        pthread_t dtid,ctid;
        int init = 1;

        myThread_Dis((void *)&init);

        sleep(3);

        while(1)
        {
        		std::cout<<"\t\t discoverying"<<std::endl;
        		std::cout<<"\t\t control"<<std::endl;
        		std::cout<<"enter the string: ";
        		std::cin >>JPP.command;
        		if(!(JPP.command.find("control")))
        		{
        			std::cout<<std::endl;
        			std::cout<<"\t\tgetting"<<std::endl;
        			std::cout<<"\t\tposting"<<std::endl;
        			std::cout<<"\t\tobserveing"<<std::endl;
        			std::cout<<"enter the string: ";
        			std::cin >>JPP.command;

        			std::cout<<"\t\t enter UUID:"<<std::endl;
           			std::cin >>JPP.UUID;

           			std::cout<<"\t\t enter deviceID:"<<std::endl;
        			std::cin >>JPP.DevID;


        			if(!(JPP.command.find("posting")))
        			{
        				std::cout<<"\t\t enter attributeName:"<<std::endl;
            			std::cin >>JPP.attributeName1;

        				std::cout<<"\t\t enter attributeValue:"<<std::endl;
            			std::cin >>JPP.attributeValue1;

        			}

        		}



                int request = convertToASCII(JPP.command);

                if(request==0)
                        continue;

                switch(request)
                {
                case discoverying:
                {
                        std::cout << "in discoverying" << '\n';
                        pthread_create(&dtid,0,myThread_Dis,(void *)&init);
                }
                break;
                default:
                {
                        pthread_create(&ctid,0,ResControl,(void *)&request);
                }
                break;
                }

        }
        return 0;
}
