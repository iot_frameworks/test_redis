#include "headers.h"

void Auto_Discovery(int);

using namespace OC;
using namespace std;

std::map<string, t_resource*> t_resourcemap;
std::map<string, t_resource*>::iterator t_resourcemapItr;      // t_resource map

std::map<string, t_resource*> t_resourceColmap;
std::map<string, t_resource*>::iterator t_resourceColmapItr; // t_resourceColmap map

std::map<string, t_things*> t_thingsmap;                         // t_things map

static OCConnectivityType connectivityType = CT_ADAPTER_IP;

std::map<std::string, int> RepPayloadMap;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;

bool auto_Discovery = false;

void foundResource(std::shared_ptr<OCResource> resource) {
//        std::cout << "In foundResource Callback start: "<< std::endl;
	t_resource *temp = new t_resource;

	temp->resource = resource;
	string hostAddress = resource->host();
	temp->resourceURI = resource->uri();
	temp->serverID = resource->sid();
	temp->key = temp->serverID + temp->resourceURI;

	if (!temp->resourceURI.compare("/oic/sec/doxm") == 0
			&& !temp->resourceURI.compare("/oic/sec/pstat") == 0
			&& !temp->resourceURI.compare("/oic/d") == 0
			&& !temp->resourceURI.compare("/oic/p") == 0
			&& !temp->resourceURI.compare("oic/d") == 0
			&& !temp->resourceURI.compare("oic/p") == 0) {
		std::vector<std::string> interfaceVec;
		for (auto &resourceInterfaces : temp->resource->getResourceInterfaces()) {
			interfaceVec.push_back(resourceInterfaces);
		}

		if (std::find(interfaceVec.begin(), interfaceVec.end(), "oic.if.ll")
				!= interfaceVec.end()) {

			t_resourceColmap[temp->key] = temp;

		} else {
			t_resourcemap[temp->key] = temp;
		}
		// std::cout << "Resource URI: " << temp->resourceURI << std::endl;
		// std::cout << "Server ID: " << temp->serverID << std::endl;
		// std::cout << "Host Address: " << hostAddress << std::endl;
	}
	alarm(1);
}

void discoveryFun() {
	std::ostringstream requestURI;
	OCStackResult r;                           /////////////////         testing
	cout << "In function Discovery" << endl;
	// Find all resources
	requestURI << OC_RSRVD_WELL_KNOWN_URI; // << "?rt=oic/res";

	r = OCPlatform::findResource("", requestURI.str(), CT_DEFAULT,
			&foundResource);
	std::cout << "Finding Resource after foundResource... " << r << std::endl;

}

void receivedDeviceInfo(const OCRepresentation& rep) {
	t_things *temp = new t_things;
	rep.getValue("di", temp->uuid);
	rep.getValue("n", temp->thingsName);
	rep.getValue("dt", temp->thingsType);

	std::string thingsName = " thingsName " + temp->thingsName;
	std::string thingsType = " thingsType " + temp->thingsType;

	// hashredis.Writeredisdiscovery( temp->uuid, thingsName );
	// hashredis.Writeredisdiscovery( temp->uuid, thingsType );

	t_thingsmap[temp->uuid] = temp;
}

void deviceInfo() {
	cout << "In function deviceInfo " << endl;
	std::string deviceDiscoveryURI = "/oic/d";
	OCPlatform::getDeviceInfo("", deviceDiscoveryURI, connectivityType,
			&receivedDeviceInfo);
}

void receivedPlatformInfo(const OCRepresentation& rep) {
//    std::cout << "\nPlatform Information received ---->\n";
	std::string PlatId;
	rep.getValue("pi", PlatId);

	auto it = t_thingsmap.find(PlatId);
	if (it != t_thingsmap.end()) {
		rep.getValue("mnmn", it->second->manufacturerName);
		rep.getValue("mnos", it->second->osName);
		rep.getValue("mnhw", it->second->hardwareVersion);
		rep.getValue("mnfv", it->second->firmwareVersion);

		std::string manufacturerName = " manufacturerName "
				+ it->second->manufacturerName;
		std::string osName = " osName " + it->second->osName;
		std::string hardwareVersion = " hardwareVersion "
				+ it->second->hardwareVersion;
		std::string firmwareVersion = " firmwareVersion "
				+ it->second->firmwareVersion;

		// hashredis.Writeredisdiscovery( PlatId, manufacturerName );
		// hashredis.Writeredisdiscovery( PlatId, osName );
		// hashredis.Writeredisdiscovery( PlatId, hardwareVersion );
		// hashredis.Writeredisdiscovery( PlatId, firmwareVersion );
	}

}

void platformInfo() {
	cout << "In function platformInfo " << endl;
	std::string platformDiscoveryURI = "/oic/p";
	OCPlatform::getPlatformInfo("", platformDiscoveryURI, connectivityType,
			&receivedPlatformInfo);
}

void Discovery_all() {
	cout << "In function Discovery_all " << endl;
	deviceInfo();
	platformInfo();
	discoveryFun();
	std::cout << " after Discovery_all " << '\n';
}

void t_things_garbage() {
	for (auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it) {
		if (!(it->second->manufacturerName.length())) {
			t_thingsmap.erase(it->first);
		}
	}
}

void t_things_device() {
	std::cout << " \nIN t_things_device : " << '\n';

	for (auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend();
			++it_uid) {
		int i = 0;
		for (auto it_key = t_resourceColmap.cbegin();
				it_key != t_resourceColmap.cend(); ++it_key) {
			if (!(it_key->first.find(it_uid->first))) {
				t_device *device = new t_device;

				it_uid->second->deviceVec.push_back(device);
				it_uid->second->deviceVec[i]->Coluri = it_key->first;
				std::cout << it_uid->first << "\t" << it_key->first << '\n';
				for (auto it_key = t_resourcemap.cbegin();
						it_key != t_resourcemap.cend(); ++it_key) {
					if (!(it_key->first.find(
							it_uid->second->deviceVec[i]->Coluri))) {
						it_uid->second->deviceVec[i]->resMap.insert(
								pair<string, t_resource*>(it_key->first,
										it_key->second));
					}
				}
				i++;
			}
		}
	}
}

void t_things_print() {
	REDIS hiredis;

	std::cout << " \n t_things Map: " << '\n';
	for (auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it) {
		std::cout << it->first << " " << it->second->uuid << " ";
		std::cout << it->second->thingsName << " " << it->second->thingsType
				<< "\n";
		std::cout << it->second->manufacturerName << " " << it->second->osName
				<< " ";
		std::cout << it->second->hardwareVersion << " "
				<< it->second->firmwareVersion << "\n";
		for (int i = 0; i < it->second->deviceVec.size(); i++) {
			cout << it->second->deviceVec[i]->Coluri << "\n";
			for (auto it_non = it->second->deviceVec[i]->resMap.cbegin();
					it_non != it->second->deviceVec[i]->resMap.cend();
					++it_non) {
				std::cout << it_non->first << "\t\t";
				std::cout << it_non->second->resource << '\n';

				cout << "\n\n\nPerforming the Print function" << endl;
				hiredis.t_resource_attPrint(it_non->first,
						it_non->second->Attdetals);
			}
		}
		cout << "\n" << endl;
	}
}

void t_resource_att_details() {
	QueryParamsMap test;
	for (auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend();
			++it_uid) {
		for (int i = 0; i < it_uid->second->deviceVec.size(); i++) {
			for (auto it_non = it_uid->second->deviceVec[i]->resMap.cbegin();
					it_non != it_uid->second->deviceVec[i]->resMap.cend();
					++it_non) {
				std::cout << "get req sent :"
						<< it_non->second->resource->get(test,
								std::bind(&t_resource::onGet, it_non->second,
										_1, _2, _3)) << endl;
				usleep(500000);
			}
		}
	}
}

void end_Discovery(int sig) {
	std::cout << " \n in end_Discovery map: " << '\n';
	std::cout << " \n Non_Collection Map: " << '\n';
	for (auto it = t_resourcemap.cbegin(); it != t_resourcemap.cend(); ++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}
	std::cout << " \n Collection Map: " << '\n';
	for (auto it = t_resourceColmap.cbegin(); it != t_resourceColmap.cend();
			++it) {
		std::cout << it->first << "\t:\t" << it->second << "\n";
	}

	t_things_garbage();
	t_things_device();
	t_resource_att_details();
// sleep(1);
	t_things_print();

	if (auto_Discovery == true) {
		signal(SIGALRM, Auto_Discovery);
		alarm(3600);
	} else {
		signal(SIGALRM, SIG_IGN);
	}
}
