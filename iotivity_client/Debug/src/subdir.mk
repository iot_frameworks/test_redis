################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Discovery.cpp \
../src/hirediswrite.cpp \
../src/iotivity_client.cpp \
../src/resource.cpp 

OBJS += \
./src/Discovery.o \
./src/hirediswrite.o \
./src/iotivity_client.o \
./src/resource.o 

CPP_DEPS += \
./src/Discovery.d \
./src/hirediswrite.d \
./src/iotivity_client.d \
./src/resource.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../../iotivity-1.2.1/resource/include -I../../hiredis -I../../iotivity_client/include -I../../iotivity-1.2.1/resource/csdk/logger/include -I../../iotivity-1.2.1/resource/csdk/stack/include -I../../iotivity-1.2.1/resource/oc_logger/include -I../../iotivity-1.2.1/resource/c_common/ocrandom/include -I../../iotivity-1.2.1/resource/c_common -Os -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


